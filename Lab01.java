package cz.cvut.fel.pjv;

import java.util.Scanner;

public class Lab01 {
   
   public void start(String[] args) {
     homework();
   }

   public static void homework(){
      System.out.println("Vyber operaci (1-soucet, 2-rozdil, 3-soucin, 4-podil):");
      Scanner sc = new Scanner(System.in);
      int operation = sc.nextInt();
      double first;
      double second;
      char operator;
      double result;
      switch (operation){
         case 1:
            System.out.println("Zadej scitanec: ");
            first = sc.nextDouble();
            System.out.println("Zadej scitanec: ");
            second = sc.nextDouble();
            operator = '+';
            result = first + second;
            break;
         case 2:
            System.out.println("Zadej mensenec: ");
            first = sc.nextDouble();
            System.out.println("Zadej mensitel: ");
            second = sc.nextDouble();
            operator = '-';
            result = first - second;
            break;
         case 3:
            System.out.println("Zadej cinitel: ");
            first = sc.nextDouble();
            System.out.println("Zadej cinitel: ");
            second = sc.nextDouble();
            operator = '*';
            result = first * second;
            break;
         case 4:
            System.out.println("Zadej delenec: ");
            first = sc.nextDouble();
            System.out.println("Zadej delitel: ");
            second = sc.nextDouble();
            if(second==0){
               System.out.println("Pokus o deleni nulou!");
                return;
            }
            operator = '/';
            result = first / second;
            break;
         default:
            System.out.println("Chybna volba!");
            return;
      }
      System.out.println("Zadej pocet desetinnych mist: ");
      int c = sc.nextInt();
      if(c<0){
         System.out.println("Chyba - musi byt zadane kladne cislo!");
         return;
      }

      String places = "%." + c + "f";
      System.out.printf(places + " " + operator + " " + places + " = " + places + "\n", first, second, result);

      sc.close();
      return;
   }
}